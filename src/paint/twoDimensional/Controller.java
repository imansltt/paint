package paint.twoDimensional;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import paint.Iman;
import paint.PaintController;

public class Controller extends Application implements PaintController {
    private Scene scene;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("2D-Paint");
        stage.setScene(new Scene(new Iman().parent , 400 , 300));
        stage.show();
    }
}
